package com.example.convertcurrency.Controllers;

import com.example.convertcurrency.services.CurrencyInfoService;
import com.example.convertcurrency.services.implementation.CurrencyInfoServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/currency/")
@CrossOrigin(origins = "*")
public class CurrencyInfoController {

    @Autowired
    CurrencyInfoServiceImpl currencyInfoService;

    //Get All Currency List
    @SuppressWarnings("rawtypes")
    @GetMapping("/getCurrencies")
    public ResponseEntity getAllCurrencyInfo() {
        try {
            return new ResponseEntity<>(currencyInfoService.getAllCurrencies(), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(e.toString(), HttpStatus.NOT_FOUND);
        }
    }

    //Get Specific Currency By Name
    @SuppressWarnings("rawtypes")
    @GetMapping("/getCurrencies/{currencyName}")
    public ResponseEntity getCurrencyInfoByName(@PathVariable String currencyName) {
        try {
            return new ResponseEntity<>(currencyInfoService.getCurrencyByName(currencyName), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(e.toString(), HttpStatus.NOT_FOUND);
        }
    }

    //Calculate  Currency
    @SuppressWarnings("rawtypes")
    @GetMapping("/calculateCurrency/{currencyName}/{rateType}/{amount}")
    public ResponseEntity convertExchangeRate(@PathVariable String currencyName, @PathVariable String rateType, @PathVariable int amount) {
        if (rateType == null || rateType.equals("")) {
            return new ResponseEntity<>("Incorrect currency rate type", HttpStatus.BAD_REQUEST);
        }

        if (amount < 0) {
            return new ResponseEntity<>("Amount must be positive number ", HttpStatus.BAD_REQUEST);
        }

        try {
            return new ResponseEntity<>(currencyInfoService.convertExchangeRate(currencyName, rateType.toLowerCase(), amount), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(e.toString(), HttpStatus.NOT_FOUND);
        }
    }


}