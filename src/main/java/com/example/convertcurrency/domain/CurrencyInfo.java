package com.example.convertcurrency.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document(collection = "CurrencyInfo")
@Getter
@Setter
@ToString
public class CurrencyInfo {
  private String name;
  private double buyRate;
  private double sellRate;
  private String  transitionDate;

  public CurrencyInfo( String name, Double buyRate, Double sellRate, String transitionDate) {
    this.name = name;
    this.buyRate = buyRate;
    this.sellRate = sellRate;
    this.transitionDate = transitionDate;
  }
}