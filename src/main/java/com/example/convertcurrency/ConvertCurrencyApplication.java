package com.example.convertcurrency;

import com.example.convertcurrency.domain.CurrencyInfo;
import com.example.convertcurrency.repository.CurrencyRepository;
import com.example.convertcurrency.services.CurrencyInfoService;
import com.example.convertcurrency.services.TimeService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.HashSet;

@SpringBootApplication
public class ConvertCurrencyApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConvertCurrencyApplication.class, args);
    }

    @Bean
    CommandLineRunner init(
            CurrencyRepository currencyRepository
    ) {

        return args -> {

            // Product Type

            try {

                CurrencyInfo checkUSDCurrency = currencyRepository.findCurrencyInfoByName("USD");
                if (checkUSDCurrency == null) {
                    CurrencyInfo usd = new CurrencyInfo("USD", 1.3392, 1.3574, new TimeService().getTimeStamp());
                    currencyRepository.save(usd);
                }

                CurrencyInfo checkHKDCurrency = currencyRepository.findCurrencyInfoByName("HKD");
                if (checkHKDCurrency == null) {
                    CurrencyInfo hkd = new CurrencyInfo("HKD", 0.1738, 0.1698, new TimeService().getTimeStamp());
                    currencyRepository.save(hkd);
                }


            } catch (Exception e) {
                System.out.println("Exception: " + e);
            }
        };
    }

}
