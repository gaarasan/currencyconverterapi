package com.example.convertcurrency.repository;

import com.example.convertcurrency.domain.CurrencyInfo;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Service;

public interface CurrencyRepository extends MongoRepository<CurrencyInfo, String> {
    //custom finder method
    CurrencyInfo findCurrencyInfoByName(String currencyName);
}
