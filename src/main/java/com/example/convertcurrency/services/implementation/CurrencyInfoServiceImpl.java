package com.example.convertcurrency.services.implementation;

import com.example.convertcurrency.domain.CurrencyInfo;
import com.example.convertcurrency.repository.CurrencyRepository;
import com.example.convertcurrency.services.CurrencyInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class CurrencyInfoServiceImpl  implements CurrencyInfoService {

    @Autowired
    public CurrencyRepository currencyRepository;


    @Override
    public List<CurrencyInfo> getAllCurrencies() {
        return currencyRepository.findAll();
    }

    @Override
    public CurrencyInfo getCurrencyByName(String currencyName) {
         CurrencyInfo currencyInfo = currencyRepository.findCurrencyInfoByName(currencyName);
        if(currencyInfo == null){
            throw new IllegalArgumentException("Currency name: " + currencyName + " is not available at the moment.");
        }
        return currencyInfo;
    }

    @Override
    public double convertExchangeRate(String currencyName, String rateType, int amount) {
        double convertedAmount;
        CurrencyInfo currencyDetails = currencyRepository.findCurrencyInfoByName(currencyName);
        if(currencyDetails == null) {
                throw new IllegalArgumentException("Currency name: " + currencyName + " cannot be empty.");
        } else {
            if (rateType.equals("buy")) {
                convertedAmount = currencyDetails.getBuyRate() * amount;
            } else {
                convertedAmount = currencyDetails.getSellRate() * amount;
            }

        }
        return  (double) Math.round(convertedAmount * 100) / 100;
    }
}