package com.example.convertcurrency.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

@Slf4j
@Service
public class TimeService {
    private String timeStamp;
    private String timeZone;
    private SimpleDateFormat format;

    public TimeService(){
        Calendar calendar = new GregorianCalendar();
        TimeZone currentTimeZone = calendar.getTimeZone();
        format = new SimpleDateFormat("yyyy-MM-dd HH:mm:s:S");
        timeStamp = format.format(new Date());
        timeZone = currentTimeZone.getDisplayName();
    }

    public String getTimeStamp() {
        timeStamp = format.format(new Date());
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public SimpleDateFormat getFormat() { return format; }

    public void setFormat(SimpleDateFormat format) { this.format = format; }

}
