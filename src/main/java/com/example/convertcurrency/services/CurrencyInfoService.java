package com.example.convertcurrency.services;

import com.example.convertcurrency.domain.CurrencyInfo;

import java.util.List;

public interface CurrencyInfoService {

    List<CurrencyInfo> getAllCurrencies();

    CurrencyInfo getCurrencyByName(String currencyName);

    double convertExchangeRate(String currencyName, String rateType, int amount);



}